Budget Planner (Ale) - create a budget for event, month or a hobby and share it with friends

# Technical specifications

The project is a simple web app (server-rendered react progressive web app). Optimized both for desktop and mobile. The landing page should be technically SEO and mobile friendly with offline support.

It will require an NodeJS API and a (Relational/NoSQL) database which will be responsible to store the users and the plans and interact with them.

Well documented, fully tested with unit and integration tests. Easily deployable with a CI and CD pipelines (Jenkins) and webpack build process.

### Phase 1 - the MVP (deadline 20.10.2017)

- Required research
    - Server-side rendering with React - NextJS and setting up the initial structure
    - Progressive web apps (app shell architecture)
    - Offline support with service workers
    - SEO optimization
    - Databases architecture
    - NodeJS catch up
    - Unit and integration testing
    - Webpack
    - Pipelines
    - Flow type checking
- Project management and planning
    - Use a simple Kanban board like Trello or something else (research about alternatives)
    - Set up milestones
    - Try to estimate it properly from the beginning
    - Tag versions and try to plan the releases
- Technical implementation
    - Set up a NodeJS API with the latest standards (es-final) and pick up a nice back-end framework
    - Design the data structure - create a nice schema representing the whole picture
    - Set up a server-side rendered progressive react web app with hydration on the client
    - Set up webpack both for the server and the client
    - Use flow type checking both on the server and on the client

### Phase 2 - the product and the business plan

- Required research
- Project management and planning
- Technical implementation
- Design
- Business